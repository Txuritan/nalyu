use {
    crate::State,
    spair::{prelude::*, Nodes, Render},
};

pub struct Index<'s> {
    pub state: &'s State,
}

impl<'s> Render<State> for Index<'s> {
    fn render(self, nodes: Nodes<State>) -> Nodes<State> {
        nodes.div(|d| {
            d.static_attributes()
                .class("grid halves")
                .nodes()
                .div(|d| {
                    d.static_attributes().class("column");
                })
                .div(|d| {
                    d.static_attributes().class("column");
                });
        })
    }
}
