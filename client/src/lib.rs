#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

mod components;
mod pages;

use {
    crate::{
        components::{Footer, Header},
        pages::Index,
    },
    spair::{prelude::*, Comp, Component, Context, Location, Routes},
};

#[derive(PartialEq)]
pub enum Filter {
    Index,
    Settings,
    Project(String),
}

impl Default for Filter {
    fn default() -> Self {
        Filter::Index
    }
}

impl Routes<State> for Filter {
    fn url(&self) -> String {
        match self {
            Filter::Index => String::from("#/"),
            Filter::Settings => String::from("#/settings"),
            Filter::Project(id) => {
                let mut url = String::from("#/project/");

                url.push_str(id.as_str());

                url
            }
        }
    }

    fn routing(location: Location, comp: &Comp<State>) {
        let hash = location.hash().unwrap_or_else(|_| String::from("#/"));

        log::trace!("Finding route filter for: {}", &hash);

        let trimmed = hash.trim_start_matches("#/");
        let mut split = trimmed.split('/');

        let filter = match (split.next(), split.next(), split.next(), split.next(), split.next()) {
            (Some(""), None, None, None, None) => {
                log::trace!("Router found `index` filter");

                Filter::Index
            },
            (Some("settings"), None, None, None, None) => {
                log::trace!("Router found `settings` filter");

                Filter::Settings
            },
            _ => {
                log::warn!("Could not find router handler, defaulting to the index");

                Filter::Index
            }
        };

        comp.update_arg(filter, &State::set_filter);
    }
}

#[derive(Default)]
pub struct State {
    filter: Filter,
}

impl State {
    fn set_filter(&mut self, filter: Filter) {
        log::trace!("Setting route filter to: {}", filter.url());

        self.filter = filter;
    }
}

impl Component for State {
    type Routes = Filter;

    fn render(&self, ctx: Context<Self>) {
        let (_, element) = ctx.into_parts();

        element
            .static_nodes()
            .render(Header)
            .nodes()
            .main(|m| match self.filter {
                Filter::Index => {
                    // m.nodes().render(Index { state: self });
                    m.nodes().h1(|h| {
                        h.static_nodes().r#static("Index");
                    });
                }
                Filter::Settings => {
                    m.nodes().h1(|h| {
                        h.static_nodes().r#static("Settings");
                    });
                }
                Filter::Project(_) => {}
            })
            .static_nodes()
            .render(Footer);
    }
}

#[wasm_bindgen(start)]
pub fn render() {
    wasm_logger::init(wasm_logger::Config::default());

    let state = State::default();

    spair::start(state, "container");
}
