# Nalyu

Nalyu is a self-hosted Docker based build service, its purpose is to built a Rust project to multiple platforms with minimal work.

## Platforms

Supported:

  - Non yet

Planned:

  - ARMv7
  - ARMv8
  - Mac
  - Linux x86
  - Linux x86_64
  - Windows x86
  - Windows x86_64
