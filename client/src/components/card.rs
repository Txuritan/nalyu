use {
    crate::{Filter, State},
    spair::{prelude::*, Nodes, Render},
};

pub struct Card<'s> {
    pub state: &'s State,
}

impl<'s> Render<State> for Card<'s> {
    fn render(self, nodes: Nodes<State>) -> Nodes<State> {
        nodes.article(|a| {})
    }
}
