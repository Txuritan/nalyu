use {
    crate::{Filter, State},
    spair::{prelude::*, Nodes, Render},
};

pub struct Header;

impl Render<State> for Header {
    fn render(self, nodes: Nodes<State>) -> Nodes<State> {
        nodes.nav(|n| {
            n.static_attributes()
                .class("nav")
                .static_nodes()
                .h1(|h| {
                    h.static_attributes()
                        .class("nav__brand")
                        .static_nodes()
                        .a(|a| {
                            a.static_attributes()
                                .href(Filter::Index)
                                .static_nodes()
                                .r#static("nalyu");
                        });
                })
                .ul(|u| {
                    u.static_attributes().class("nav__left");
                })
                .ul(|u| {
                    u.static_attributes()
                        .class("nav__right")
                        .static_nodes()
                        .li(|l| {
                            l.static_attributes()
                                .class("nav__item")
                                .static_nodes()
                                .a(|a| {
                                    a.static_attributes()
                                        .href(Filter::Settings)
                                        .static_nodes()
                                        .r#static("settings");
                                });
                        });
                });
        })
    }
}
