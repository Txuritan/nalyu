use {
    crate::{Filter, State},
    spair::{prelude::*, Nodes, Render},
};

pub struct Footer;

impl Render<State> for Footer {
    fn render(self, nodes: Nodes<State>) -> Nodes<State> {
        nodes.footer(|f| {
            f.static_attributes()
                .class("nav")
                .static_nodes()
                .div(|d| {
                    d.static_attributes().class("nav__left");
                })
                .ul(|u| {
                    u.static_attributes()
                        .class("nav__right")
                        .static_nodes()
                        .li(|l| {
                            l.static_attributes()
                                .class("nav__item")
                                .static_nodes()
                                .a(|a| {
                                    a.static_attributes()
                                        .href(Filter::Index)
                                        .static_nodes()
                                        .r#static(concat!(
                                            "v",
                                            env!("CARGO_PKG_VERSION"),
                                            "-",
                                            env!("GIT_VERSION")
                                        ));
                                });
                        });
                });
        })
    }
}
